﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure;

namespace GameLibrary
{
    public abstract class AbstractFactory
    {
        public abstract IGame NewGame();
        public abstract string Name { get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Infrastructure;
using System.Diagnostics;

namespace GameLibrary
{
    public class ReflexGame : IGame
    {
        public event EventHandler<MessagesEventArgs> Messages;
        public event EventHandler<GameFinishedEventArgs> GameFinished;
        private Stopwatch _stopwatch = new Stopwatch();
        private Timer timer = new Timer(new Random().Next(2, 6) * 1000);
        public bool Guess(string bet)
        {
            Result = (int) _stopwatch.ElapsedMilliseconds;
            if (Counter == 0)
            {
                Counter++;
                timer.AutoReset = false;
                timer.Start();
                timer.Elapsed += OnTimeEvent;
                return false;
            }
           if(_stopwatch.IsRunning)
           {
                GameFinished?.Invoke(this, new GameFinishedEventArgs(true, (int)_stopwatch.ElapsedMilliseconds));
               return true;
           }                
            
           GameFinished?.Invoke(this, new GameFinishedEventArgs(false, 0));
            timer.Close();
            return false;
        }

        

        private void OnTimeEvent(object sender, ElapsedEventArgs e)
        {
            Messages?.Invoke(this, new MessagesEventArgs("\nTeraz"));
            _stopwatch.Start();
        }

        
        public int Counter { get; private set; }
        public int Result { get; private set; }
        //public string Name => "Reflex game";

        public class ReflexGameFactory : AbstractFactory
        {
            public override string Name => "Reflex game";

            public override IGame NewGame()
            {
                return new ReflexGame();
            }
        }

    }
}

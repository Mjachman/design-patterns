﻿using System;
using Infrastructure;


namespace GameLibrary
{
    public class GuessGame : IGame
    {
        
        public event EventHandler<MessagesEventArgs> Messages;
        public event EventHandler<GameFinishedEventArgs> GameFinished;
        private readonly int _number = 100;//new Random().Next(0,1001);

        public bool Guess(string bet)
        {
            Result++;

            var cmp = _number.CompareTo(Int32.Parse(bet));
            if (cmp > 0)
            {
                Messages?.Invoke(this, new MessagesEventArgs("Wiecej"));
            }
            else if (cmp < 0)
            {
                Messages?.Invoke(this, new MessagesEventArgs("Mniej"));
            }
            else
            {
                Messages?.Invoke(this, new MessagesEventArgs("Zgadles"));
                GameFinished?.Invoke(this, new GameFinishedEventArgs(true, Result));
                return true;
            }
            return false;
        }
        public class GuessGameFactory : AbstractFactory
        {
            public override string Name => "Guess game";

            public override IGame NewGame()
            {
                return new GuessGame();
            }
        }

        public int Result { get; private set; }

        public string Name => "Guess game";
    }
}

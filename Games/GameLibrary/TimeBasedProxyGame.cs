﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure;

namespace GameLibrary
{
    public abstract class TimeBasedProxyGame : IGame
    {
        private readonly IGame _chosenGame;
        public event EventHandler<MessagesEventArgs> Messages;
        public event EventHandler<GameFinishedEventArgs> GameFinished;
        private readonly Stopwatch _stopwatch = new Stopwatch();


        public TimeBasedProxyGame(IGame game)
        {
            _chosenGame = game;
            _chosenGame.Messages += InitiateNextEvent;
            _chosenGame.GameFinished += InitiateNextEvent2;
        }

        private void InitiateNextEvent2(object sender, GameFinishedEventArgs e)
        {
            this.Result = (int)_stopwatch.ElapsedMilliseconds;
            _stopwatch.Stop();
            var args = new GameFinishedEventArgs(e.IsWon, Result);
            this.GameFinished?.Invoke(this, args);
        }

        private void InitiateNextEvent(object sender, MessagesEventArgs e)
        {
            this.Messages?.Invoke(sender, e);
        }


        public bool Guess(string bet)
        {
            _stopwatch.Start();
            return _chosenGame.Guess(bet);
        }

        public int Result { get; private set; }
        //public string Name => $"{_chosenGame.Name} - time-based";
        public class TimeBasedGuessGameFactory : AbstractFactory
        {
            public override string Name => "Guess game-time based";

            public override IGame NewGame()
            {
                return new TimeBasedGuessGame();
            }
        }
        public class TimeBasedMastermindGameFactory : AbstractFactory
        {
            public override string Name => "Mastermind game-time based";

            public override IGame NewGame()
            {
                return new TimeBasedMastermindGame();
            }
        }
    }
}

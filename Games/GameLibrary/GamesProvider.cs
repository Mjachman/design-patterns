﻿using System;
using System.Linq;
using Infrastructure;

namespace GameLibrary
{
    public class GamesProvider : IGamesProvider
    {
        //private readonly IGame[] _games;
        private readonly AbstractFactory[] _gameFactories;
        private IGame _chosenGame;
        public event EventHandler<MessagesEventArgs> Messages2;
        public event EventHandler<GameFinishedEventArgs> GameFinished2;

        public GamesProvider(AbstractFactory[] gameFactories)
        {
            _gameFactories = gameFactories;
        }

        public string[] GetGameNames()
        {
            //return _games.Select(g => g.Name).ToArray();
            return _gameFactories.Select(g => g.Name).ToArray();
        }

        public void ChooseGame(int n)
        {
            //_chosenGame = _games[n];
            _chosenGame = _gameFactories[n].NewGame();
        }

        public void ChooseGame(string name)
        {
            //_chosenGame = _games.First(g => g.Name == name);
            _chosenGame = _gameFactories.First(g => g.Name == name).NewGame();
            _chosenGame.Messages += InitiateNextEvent;
            _chosenGame.GameFinished += InitiateNextEvent2;
        }

        public bool Guess(string bet)
        {
            return _chosenGame.Guess(bet);
        }

        public int Result
        {
            get { return _chosenGame.Result; }
        }

        public void InitiateNextEvent(object oSender, MessagesEventArgs oMessagesEventArgs)
        {
            Messages2?.Invoke(this, oMessagesEventArgs);
        }

        public void InitiateNextEvent2(object oSender, GameFinishedEventArgs oGameFinishedEventArgs)
        {
            GameFinished2?.Invoke(this, oGameFinishedEventArgs);
        }
    }
}

﻿using System.Collections.Specialized;
using System.Configuration;
using HighScore;
using Infrastructure;
using Ninject.Activation;

namespace DodatkowaPraca
{
    internal class ScoreManagerProvider : Provider<IScoreManager>
    {
        protected override IScoreManager CreateInstance(IContext context)
        {
            var selectprovider = ((NameValueCollection)ConfigurationManager.GetSection("ScoreClassConfig"))["ChooseSqlProvider"];

            if (selectprovider == "true")
            {
                var connectionString = ((NameValueCollection)ConfigurationManager.GetSection("ScoreClassConfig"))["ConnectionString"];
                return new SqlScoreManager(connectionString, 10);
            }
            else
            {
                var filepath = ((NameValueCollection)ConfigurationManager.GetSection("ScoreClassConfig"))["FilePath"];
                return new FileScoreManager(filepath, 10);
            }
            
        }
    }
}

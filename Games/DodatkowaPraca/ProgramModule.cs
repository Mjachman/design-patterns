﻿using Ninject.Modules;
using Infrastructure;
using GameLibrary;

namespace DodatkowaPraca
{
    class ProgramModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IGamesProvider>().To<GamesProvider>().InSingletonScope();
            Bind<IScoreManager>().ToProvider(new ScoreManagerProvider());

            Bind<AbstractFactory>().To<GuessGame.GuessGameFactory>();
            Bind<AbstractFactory>().To<MastermindGame.MastermindGameFactory>();
            Bind<AbstractFactory>().To<ReflexGame.ReflexGameFactory>();
            Bind<AbstractFactory>().To<TimeBasedProxyGame.TimeBasedGuessGameFactory>();
            Bind<AbstractFactory>().To<TimeBasedProxyGame.TimeBasedMastermindGameFactory>();
        }
    }
}

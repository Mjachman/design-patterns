﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Infrastructure;

namespace HighScore
{
    public class SqlScoreManager : IScoreManager
    {
        private readonly string _connectionString;
        private readonly int _howMany;
        

        public SqlScoreManager(string connectionString, int howMany)
        {
            _connectionString = connectionString;
            _howMany = howMany;
            GamesProvider.GameFinished2 += GameOver;
        }

        public HighscoreEntry[] List()
        {
            var lista = new List<HighscoreEntry>();

            WithCommand($"SELECT TOP {_howMany} * FROM Highscore ORDER BY wynik;", command =>
            {
                using (var dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        var modelClass = new HighscoreEntry(dataReader.GetValue(1).ToString(), Convert.ToInt32(dataReader.GetValue(2).ToString()));
                        lista.Add(modelClass);
                    }
                }
            });

            return lista.ToArray();
        }

        public void Add(HighscoreEntry entry)
        {
            WithCommand("INSERT INTO Highscore VALUES (@b, @c);", command =>
            {
                command.Parameters.AddWithValue("@b", entry.Name);
                command.Parameters.AddWithValue("@c", entry.Score);
                command.ExecuteNonQuery();
            });
        }

        private void WithCommand(string cmd, Action<SqlCommand> action)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var command = new SqlCommand(cmd, connection))
            {
                connection.Open();
                action(command);
            }
        }

        public void GameOver(object oSender, GameFinishedEventArgs oGameFinishedEventArgs)
        {
            if (!oGameFinishedEventArgs.IsWon) return;
            var highscoreEntry = new HighscoreEntry(Name, GamesProvider.Result);
            Add(highscoreEntry);
            Console.WriteLine("\nNAJLEPSZE WYNIKI:");
            foreach (var entry in List())
            {
                Console.WriteLine(entry);
            }
        }
        public string Name { get; set; }
        public IGamesProvider GamesProvider { get; set; }
    }
}

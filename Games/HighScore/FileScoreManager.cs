﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Infrastructure;

namespace HighScore
{
    public class FileScoreManager : IScoreManager
    {
        private readonly string _filePath;
        private readonly int _howMany;

        public FileScoreManager(string filepath, int howmany)
        {
            _filePath = filepath;
            _howMany = howmany;
        }



        public HighscoreEntry[] List()
        {
            var list = new List<HighscoreEntry>();
            var content = File.ReadAllLines(_filePath);
            foreach (var line in content)
            {
                var highscore = line.Split('-');
                list.Add(new HighscoreEntry(highscore[0].ToString(),Convert.ToInt32(highscore[1])));
            }

            return list.OrderBy(a => a.Score).Take(_howMany).ToArray();
        }

        public void Add(HighscoreEntry entry)
        {
            File.AppendAllLines(_filePath, new [] { $"{entry.Name}-{entry.Score}"});
        }

        //public void GameOver(object oSender, GameFinishedEventArgs oGameFinishedEventArgs)
        //{
        //    if (!oGameFinishedEventArgs.IsWon) return;
        //    var highscoreEntry = new HighscoreEntry(Name, GamesProvider.Result);
        //    Add(highscoreEntry);
        //    Console.WriteLine("\nNAJLEPSZE WYNIKI:");
        //    foreach (var entry in List())
        //    {
        //        Console.WriteLine(entry);
        //    }
        //}
        public string Name { get; set; }
    }
}

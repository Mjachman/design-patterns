﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Infrastructure;

namespace HighScore
{
    class HighScoreFile : IDisposable
    {
        private readonly FileStream _stream;

        public HighScoreFile(string path)
        {
            _stream = File.Open(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
        }

        IEnumerable<HighscoreEntry> ReadEntries()
        {
            _stream.Seek(0, SeekOrigin.Begin);

            var reader = new StreamReader(_stream);

            return reader.ReadToEnd()
                .Split('\n', '\r')
                .Select(l =>
                {
                    var tokens = l.Split('-');
                    return new HighscoreEntry(tokens[0], int.Parse(tokens[1]));
                })
                .ToList();
        }

        void Append(HighscoreEntry entry)
        {
            _stream.Seek(0, SeekOrigin.End);

            using (var writer = new StreamWriter(_stream, Encoding.Default, 0, true))
            {
                writer.WriteLine($"{entry.Name}-{entry.Score}");
            }
        }

        public void Dispose()
        {
            _stream.Dispose();
        }
    }
}